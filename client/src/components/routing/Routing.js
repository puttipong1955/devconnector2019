import React from "react";
import Login from "../auth/Login";
import Register from "../auth/Register";
import Alert from "../layout/Alert";
import { Route, Switch } from "react-router-dom";
import Dashboard from "../dashboard/Dashboard";
import PrivateRout from "../routing/PrivateRouting";
import CreactProfile from "../profile-forms/CreateProfile";
import EditProfile from "../profile-forms/EditProfile";
import AddExperience from "../profile-forms/AddExperience";
import AddEducation from "../profile-forms/AddEducation";
import Profiles from "../profiles/Profiles";
import Profile from "../profile/Profile";
import Posts from "../posts/Posts";
import Post from "../post/Post";
import NotFound from "../layout/NotFound";

const Routing = () => {
  return (
    <section className="container">
      <Alert />
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/profiles" component={Profiles} />
        <Route exact path="/profile/:id" component={Profile} />
        <PrivateRout exact path="/dashboard" component={Dashboard} />
        <PrivateRout exact path="/create-profile" component={CreactProfile} />
        <PrivateRout exact path="/edit-profile" component={EditProfile} />
        <PrivateRout exact path="/add-experience" component={AddExperience} />
        <PrivateRout exact path="/add-education" component={AddEducation} />
        <PrivateRout exact path="/posts" component={Posts} />
        <PrivateRout exact path="/posts/:id" component={Post} />
        <Route component={NotFound} />
      </Switch>
    </section>
  );
};

export default Routing;
